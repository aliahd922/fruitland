class CheckoutController < ApplicationController
  protect_from_forgery

  def new
    @plan = params[:plan].merge!(plan_name: params[:plan_name])
    @checkout = Checkout.new(@plan)
  end

  def create
    @checkout = Checkout.new(checkout_params)
    if @checkout.save
      flash[:notice] = "Thank you for purchasing item."
      redirect_to @checkout.paypal_url(params[:checkout][:business_email], checkout_path(@checkout.id))
    else
      render :new
    end
  end

  def show
    redirect_to root_path
  end

  def checkout_params
    params.require(:checkout).permit(:full_name, :address, :email, :plan_name, :plan_price)
  end
end
