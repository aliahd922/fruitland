class HomeController < ApplicationController
  protect_from_forgery except: [:hook]
  
  def index
  end

  def hook
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @checkout = Checkout.find params[:invoice]
      @checkout.update_attributes notification_params: params, status: status, transaction_id: params[:txn_id], purchased_at: Time.now
    end
    render nothing: true
  end
end
