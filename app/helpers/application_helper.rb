module ApplicationHelper
  def plan_name_class(plan_name)
    case plan_name
    when "gold"
      "warning"
    when "brownz"
      "dull"
    else
      "success"
    end
  end

  def validation_errors_notifications(object)
    return '' if object.errors.empty?

    messages = object.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
      <div class="alert alert-danger alert-block"> <button type="button"
      class="close" data-dismiss="alert">x</button>
      #{messages}
      </div>
    HTML

    html.html_safe
  end

  def alert_class_for flash_type
    case flash_type.to_sym
      when :success
        "alert-success"
      when :error
        "alert-danger"
      when :alert
        "alert-warning"
      when :notice
        "alert-info"
    end
  end

  def icon_class_for flash_type
    case flash_type.to_sym
      when :success
        "glyphicon-ok"
      when :error
        "glyphicon-remove"
      when :alert
        "glyphicon-remove"
      when :notice
        "glyphicon-ok"
    end
  end
end
