class Checkout < ApplicationRecord
  validates_presence_of :full_name, :email, :plan_name, :plan_price

  def paypal_url(business_email, return_path)
    values = {
            business: business_email,
            cmd: "_xclick",
            upload: 1,
            return: "#{Rails.application.secrets.app_host}#{return_path}",
            invoice: id,
            amount: 4000,
            item_name: self.plan_name.humanize,
            item_number: rand(50),
            quantity: '1',
            notify_url: "#{Rails.application.secrets.app_host}/hook"
        }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end
end
