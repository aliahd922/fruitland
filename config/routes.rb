Rails.application.routes.draw do
  post 'transaction' => "checkout#create", as: :proceed_checkout
  get 'checkout/new/:plan_name' => "checkout#new", as: :new_checkout

  post "/checkout/:id" => "checkout#show", as: :checkout
  post "/hook" => "home#hook"

  get 'home/index'

  root to: "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
