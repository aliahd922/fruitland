class CreateCheckouts < ActiveRecord::Migration[5.0]
  def change
    create_table :checkouts do |t|
      t.string :full_name
      t.string :address
      t.string :email
      t.text :notification_params
      t.string :status
      t.string :transaction_id
      t.datetime :purchased_at
      t.string :plan_name
      t.string :plan_price

      t.timestamps
    end
  end
end
